package pw.alexbenic.myplaces

import android.net.Uri
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class MyPlacesHTTPHelper {

    companion object {
        val GET_MY_PLACE : String = "1"
        val GET_ALL_PLACES_NAME : String = "2"
        val SEND_MY_PLACE : String = "3"

        fun sendMyPlace(place : MyPlace) : String {
            var ret = ""
            try {
                var url : URL = URL("http://10.8.0.2:8080")
                var conn : HttpURLConnection = url.openConnection() as HttpURLConnection
                with(conn) {
                    connectTimeout = 150000
                    readTimeout = 10000
                    requestMethod = "POST"
                    doInput = true
                    doOutput = true
                }

                var holder : JSONObject = JSONObject()
                var data : JSONObject = JSONObject()

                with(data) {
                    put("name", place.name)
                    put("desc", place.description)
                    put("lat", place.latitude)
                    put("lon", place.longitude)
                }
                holder.put("myplace", data)

                val builder : Uri.Builder = Uri.Builder()
                        .appendQueryParameter("req", SEND_MY_PLACE)
                        .appendQueryParameter("name", place.name)
                        .appendQueryParameter("data", holder.toString())

                val query : String = builder.build().encodedQuery

                var ostream : OutputStream = conn.outputStream
                var bufferedWriter : BufferedWriter = BufferedWriter(OutputStreamWriter(ostream, "UTF-8"))
                with(bufferedWriter) {
                    write(query)
                    flush()
                    close()
                }
                ostream.close()

                val responseCode : Int = conn.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    ret = inputStreamToString(conn.inputStream)
                } else {
                    ret = "Error: ${responseCode.toString()}"
                }

            } catch ( e: Exception) {
                e.printStackTrace()
                ret = "Error during upload"
            }

            return ret
        }

        fun inputStreamToString(istream : InputStream) : String {
            var line = ""
            var total = StringBuilder()
            var rd = BufferedReader( InputStreamReader(istream))

            try {
                while (true) {
                    line = rd.readLine()
                    if ( line == null ) {
                        break
                    } else {
                        total.append(line)
                    }
                }
            } catch (e : Exception) {
                e.printStackTrace()
            }
            istream.close()
            return total.toString()
        }

        fun getMyPlace(itemName : String) : MyPlace? {
            var place : MyPlace? = null
            try {
                val url : URL = URL("http://10.8.0.2:8080")
                val conn : HttpURLConnection = url.openConnection() as HttpURLConnection
                with(conn) {
                    connectTimeout = 150000
                    readTimeout = 10000
                    requestMethod = "POST"
                    doInput = true
                    doOutput = true
                }

                val builder : Uri.Builder = Uri.Builder()
                        .appendQueryParameter("req", GET_MY_PLACE)
                        .appendQueryParameter("name", itemName)
                val query : String = builder.build().encodedQuery

                val ostream : OutputStream = conn.outputStream
                val bufferedWriter : BufferedWriter = BufferedWriter(OutputStreamWriter(ostream, "UTF-8"))
                with(bufferedWriter) {
                    write(query)
                    flush()
                    close()
                }
                ostream.close()

                val responseCode : Int = conn.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    val str = inputStreamToString(conn.inputStream)
                    val obj = JSONObject(str)
                    val jplace = obj.getJSONObject("myplace")

                    val name = jplace.getString("name")
                    val desc = jplace.getString("desc")
                    val lon = jplace.getString("lon")
                    val lat = jplace.getString("lat")

                    place = MyPlace(name, desc)
                    place.longitude = lon
                    place.latitude = lat

                }
            } catch ( e: Exception) {
                e.printStackTrace()
            }

            return place
        }
        fun getAllPlacesNames() : List<String> {
            var names : MutableList<String> = mutableListOf<String>()
            try {
                val url : URL = URL("http://10.8.0.2:8080")
                val conn : HttpURLConnection = url.openConnection() as HttpURLConnection
                with(conn) {
                    connectTimeout = 150000
                    readTimeout = 10000
                    requestMethod = "POST"
                    doInput = true
                    doOutput = true
                }

                val builder : Uri.Builder = Uri.Builder()
                        .appendQueryParameter("req", GET_ALL_PLACES_NAME)
                val query : String = builder.build().encodedQuery

                val ostream : OutputStream = conn.outputStream
                val bufferedWriter : BufferedWriter = BufferedWriter(OutputStreamWriter(ostream, "UTF-8"))
                with(bufferedWriter) {
                    write(query)
                    flush()
                    close()
                }
                ostream.close()

                val responseCode : Int = conn.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    val str = inputStreamToString(conn.inputStream)
                    val obj = JSONObject(str)
                    val jarr = obj.getJSONArray("myplacesnames")
                    for(i in 0..(jarr.length() - 1)) {
                        // look here if server doesn't respond
                        val name = jarr.getString(i)
//                        val name = jarr.getJSONObject(i).getString("name")
                        names.add(name)
                    }
                } else {
                    names.add("Connection error: ${responseCode.toString()}")
                }

            } catch ( e: Exception) {
                e.printStackTrace()
                names.add("Problem downloading places names list")
            }

            return names.toList()
        }
    }
}

