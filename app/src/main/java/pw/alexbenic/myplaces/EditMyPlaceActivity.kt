package pw.alexbenic.myplaces

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class EditMyPlaceActivity : AppCompatActivity(), View.OnClickListener {

    private var editMode : Boolean = true
    private var position : Int? = null

    val finishedButton by lazy { find<Button>(R.id.editmyplace_finished_button) }
    val cancelButton by lazy { find<Button>(R.id.editmyplace_cancel_button) }
    val locationButton by lazy { find<Button>(R.id.editmyplace_location_button) }
    val nameEditText by lazy { find<EditText>(R.id.editmyplace_name_edit) }
    val descEditText by lazy { find<EditText>(R.id.editmyplace_desc_edit) }
    val latEditText by lazy { find<EditText>(R.id.editmyplace_lat_edit) }
    val lonEditText by lazy { find<EditText>(R.id.editmyplace_lon_edit) }
    val toolbar by lazy { find<Toolbar>(R.id.toolbar) }

    override fun onCreate(savedInstanceState: Bundle?) {
        // superinit
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_my_place)
        setSupportActionBar(toolbar)

        // validators
        val nameValidator : TextWatcher = validator(nameEditText, Regex("[[:alnum:]]+"))
        val lonValidator : TextWatcher = validator(lonEditText, Regex("[[:digit:]]+"))
        val latValidator : TextWatcher = validator(lonEditText, Regex("[[:digit:]]+"))
        // validation
        nameEditText.addTextChangedListener(nameValidator)
        lonEditText.addTextChangedListener(lonValidator)
        latEditText.addTextChangedListener(latValidator)

        // edit mode
        position = getIntent()?.extras?.getInt("position")
        if (position == null) editMode = false

        if (!editMode) {
            with(finishedButton) {
                isEnabled = true
                text = "Add"
            }
        } else if (position!! >= 0) {
            finishedButton.text = "Save"
            val place = MyPlacesData.instance.getPlace(position!!)
            nameEditText.setText(place.name)
            descEditText.setText(place.description)
            latEditText.setText(place.latitude)
            lonEditText.setText(place.longitude)
        }

        // conf cancel button
        finishedButton.setOnClickListener(this)
        cancelButton.setOnClickListener(this)
        locationButton.setOnClickListener(this)


        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.menu_edit_my_place, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.show_map_item -> toast("Show Map!")
            R.id.my_places_list_item -> startActivity<MyPlacesActivity>()
            R.id.about_item -> startActivity<AboutActivity>()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(p0: View?) = when (p0?.id){
        R.id.editmyplace_cancel_button -> {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        R.id.editmyplace_location_button -> {
            val i : Intent = Intent(this, MyPlacesMapActivity::class.java)
            i.putExtra("state", MyPlacesMapActivity.SELECT_COORDINATES)
            startActivityForResult(i, 1)
        }
        R.id.editmyplace_finished_button -> {
            val name = find<EditText>(R.id.editmyplace_name_edit).text.toString()
            val desc = find<EditText>(R.id.editmyplace_desc_edit).text.toString()
            val lon = find<EditText>(R.id.editmyplace_lon_edit).text.toString()
            val lat = find<EditText>(R.id.editmyplace_lat_edit).text.toString()
            if (!editMode) {
                val place = MyPlace(name, desc)
                place.latitude = lat
                place.longitude = lon
                MyPlacesData.instance.addNewPlace(place)

            } else {
                var place = MyPlacesData.instance.getPlace(position!!)
                place.name = name
                place.description = desc
                place.longitude = lon
                place.latitude = lat

                MyPlacesData.instance.updatePlace(place)
            }

            setResult(Activity.RESULT_OK)
            finish()
        }
        else -> finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (resultCode == Activity.RESULT_OK) {
                val lon = data?.extras?.getString("lon")
                val lat = data?.extras?.getString("lat")

                lonEditText.setText(lon)
                latEditText.setText(lat)
            }
        }
        catch(e : Exception) {
            e.printStackTrace()
        }
    }

    private fun validator(tv : TextView, re : Regex) : TextWatcher {
        return object : TextValidator(tv) {
            override fun validate(textView: TextView, text: String) {
                with(textView.text.toString()) {
                    if (this.isEmpty()) textView.setError("Can't be empty!")
                    else if (!this.matches(re)) textView.setError("Can only contain numbers")
                }

            }
        }
    }


    inline fun <reified T : View> Activity.find(id: Int) : T = findViewById(id) as T

    inline fun toast(text : String, length : Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, text, length).show()
    }

    inline fun <reified T : Activity> Context.startActivity(bundle : Bundle? = null) {
        val i = Intent(this, T::class.java)
        if (bundle != null) startActivity(i.putExtras(bundle)) else startActivity(i)
    }
}

abstract class TextValidator(private val textView : TextView) : TextWatcher {
    abstract fun validate(textView : TextView, text : String)
    override fun afterTextChanged(p0: Editable?) {
        val text : String = textView.text.toString()
        validate(textView, text)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
}

