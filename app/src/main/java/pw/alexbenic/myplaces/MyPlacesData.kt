package pw.alexbenic.myplaces

class MyPlacesData( private var myPlaces : MutableList<MyPlace> = arrayListOf(),
                    private val db : MyPlacesDBAdapter = MyPlacesDBAdapter((MyPlacesApplication.context!!)))
{
    init {
        db.open()
        myPlaces = db.getAllEntries()
        db.close()
    }

    // singleton
    companion object {
         val instance : MyPlacesData = MyPlacesData()
    }

    fun getPlace(index : Int) : MyPlace = this.myPlaces[index]

    fun getMyPlaces() : MutableList<MyPlace> = this.myPlaces

    fun addNewPlace(place : MyPlace) {
        myPlaces.add(place)
        withDB {
            place.ID = db.insertEntry(place) ?: throw Exception("pw.alexbenic.myplaces.myplacesdata : DB fail entry")
        }
    }

    fun deletePlace(index : Int) {
        val place = myPlaces.removeAt(index)
        withDB {
            db.removeEntry(place.ID)
        }
    }

    fun updatePlace(place: MyPlace) {
        withDB {
            db.updateEntry(place.ID, place)
        }
    }

    private fun withDB(f : () -> Unit){
        db.open()
        f()
        db.close()
    }

}
