package pw.alexbenic.myplaces

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.TextView

class ViewMyPlaceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_my_place)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        var position : Int? = getIntent()?.extras?.getInt("position")

        if(position != null) {
            // get instance of data class
            val place = MyPlacesData.instance.getPlace(position)
            // set name
            var tw = find<TextView>(R.id.viewmyplace_name_text)
            tw.text = place.name
            // set desc
            var twd = find<TextView>(R.id.viewmyplace_desc_text)
            twd.text = place.description

            var twla = find<TextView>(R.id.viewmyplace_lat_text)
            twla.text = place.latitude

            var twlo = find<TextView>(R.id.viewmyplace_lon_text)
            twlo.text = place.longitude
        }

        val finishedButton = find<Button>(R.id.viewmyplace_finished_button)
        finishedButton.setOnClickListener { finish() }
    }

    inline fun <reified T : View> Activity.find(id: Int) : T = findViewById(id) as T
}
