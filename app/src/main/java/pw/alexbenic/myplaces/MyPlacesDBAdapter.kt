package pw.alexbenic.myplaces

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.util.Log


class MyPlacesDBAdapter(private val context : Context) {
    private var db : SQLiteDatabase? = null
    private var dbH : MyPlacesDatabaseHelper?

    init {
       dbH = MyPlacesDatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION)
    }

    companion object {
        val DATABASE_NAME = "MyPlacesDB"
        val DATABASE_TABLE = "MyPlaces"
        val DATABASE_VERSION = 1

        val PLACE_ID = "ID"
        val PLACE_NAME = "Name"
        val PLACE_DESCRIPTION = "Desc"
        val PLACE_LONG = "Long"
        val PLACE_LAT = "Lat"
    }


    fun open(): MyPlacesDBAdapter {
        this.db = this.dbH?.writableDatabase ?: throw Exception("Cannot open DB")
        return this
    }

    fun close() {
        this.db?.close()
    }

    fun insertEntry(myPlace: MyPlace) : Long? {
        var values = ContentValues()

        values.put(PLACE_NAME, myPlace.name)
        values.put(PLACE_DESCRIPTION, myPlace.description)
        values.put(PLACE_LONG, myPlace.longitude)
        values.put(PLACE_LAT, myPlace.latitude)

        var id : Long? = null

        db?.beginTransaction()
        try {
            id = this.db?.insert(DATABASE_TABLE, null, values)
            this.db?.setTransactionSuccessful()
        } catch(e : SQLiteException) {
            Log.v("MyPlacesDBAdapter", e.message)
        } finally {
            this.db?.endTransaction()
        }

        return id
    }

    fun removeEntry(id : Long) : Boolean {
        var success = false
        this.db?.beginTransaction()
        try {
            // bevare maybe dragons?! this can throw null exception; sigh...
            success = this.db?.delete(DATABASE_TABLE, "${PLACE_ID}=${id}", null)!! > 0
            this.db?.setTransactionSuccessful()
        } catch(e : SQLiteException) {
            Log.v("MyPlacesDBAdapter", e.message)
        } finally {
            this.db?.endTransaction()
        }
        return success
    }

    fun getAllEntries() : MutableList<MyPlace> {
        var myPlaces : MutableList<MyPlace> = arrayListOf<MyPlace>()
        var cursor : Cursor? = null

        this.db?.beginTransaction()
        try {
            // bevare maybe dragons?! this can throw null exception; sigh...
            cursor = this.db?.query(DATABASE_TABLE, null, null, null, null, null, null)
            this.db?.setTransactionSuccessful()
        } catch(e : SQLiteException) {
            Log.v("MyPlacesDBAdapter", e.message)
        } finally {
            this.db?.endTransaction()
        }

        if (cursor != null) {

            while(cursor.moveToNext()) {
                var myPlace = MyPlace(cursor.getString(PLACE_NAME))
                myPlace.ID = cursor.getLong(PLACE_ID)
                myPlace.description = cursor.getString(PLACE_DESCRIPTION)
                myPlace.longitude = cursor.getString(PLACE_LONG)
                myPlace.latitude = cursor.getString(PLACE_LAT)
                myPlaces.add(myPlace)
            }

        }
        return  myPlaces
    }

    fun updateEntry(id : Long, myPlace: MyPlace) : Int? {
        val where = "${PLACE_ID} = ${id}"

        var contentValue = ContentValues()

        contentValue.put(PLACE_NAME, myPlace.name)
        contentValue.put(PLACE_DESCRIPTION, myPlace.description)
        contentValue.put(PLACE_LONG, myPlace.longitude)
        contentValue.put(PLACE_NAME, myPlace.latitude)

        return this.db?.update(DATABASE_TABLE, contentValue, where, null)
    }

    inline fun Cursor.getString(column : String) : String {
        return this.getString(this.getColumnIndex(column))
    }
    inline fun Cursor.getLong(column : String) : Long {
        return this.getLong(this.getColumnIndex(column))
    }


}
