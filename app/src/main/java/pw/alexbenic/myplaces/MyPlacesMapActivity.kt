package pw.alexbenic.myplaces

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions


class MyPlacesMapActivity : AppCompatActivity(), OnMapReadyCallback {

    private val TAG : String = "MyPlacesMapActivity"
    private val DEFAULT_LOCATION : LatLng = LatLng(43.3209, 21.8958)
    private val DEFAULT_ZOOM : Float = 13F
    private var setCoorsEnabled : Boolean = false
    private var markerPlaceIdMap : HashMap<Marker, Int> = hashMapOf()

    private var locationManager : LocationManager? = null

    companion object {
        val SHOW_MAP = 1
        val CENTER_PLACE_ON_MAP = 2
        val SELECT_COORDINATES = 3

    }

    private var map : GoogleMap? = null
    private var lastLoc : Location? = null
    private var placeLoc : LatLng? = null

    private var state : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_places_map)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        try{
            val map = intent.extras
            if ( map != null) {
                state = map.getInt("state")
                if (state == CENTER_PLACE_ON_MAP) {
                    val lat = map.getString("lat").toDouble()
                    val lon = map.getString("lon").toDouble()
                    placeLoc = LatLng(lat, lon)
                }
            }
        }
        catch (e : Exception) {
            e.printStackTrace()
        }

        val fragment = getSupportFragmentManager().findFragmentById(R.id.map) as SupportMapFragment
        fragment.getMapAsync(this)

        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        lastLoc = locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

    }


    override fun onMapReady(gmap: GoogleMap) {
        map = gmap

        map?.mapType = GoogleMap.MAP_TYPE_NORMAL
        map?.uiSettings?.isZoomGesturesEnabled = true

        if (lastLoc != null) {
            map?.moveCamera(CameraUpdateFactory.newLatLngZoom( LatLng(lastLoc!!.latitude, lastLoc!!.longitude), DEFAULT_ZOOM))
        } else {
            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM))
        }


        when (state) {
            SHOW_MAP -> {
                map?.isMyLocationEnabled = true
                map?.uiSettings?.isMyLocationButtonEnabled = true
            }
            SELECT_COORDINATES -> {
                    map?.setOnMapClickListener({ latLng: LatLng ->
                        if (setCoorsEnabled) {
                            val lon = latLng.longitude.toString()
                            val lat = latLng.latitude.toString()

                            val i = Intent()
                            i.putExtra("lon", lon)
                            i.putExtra("lat", lat)
                            setResult(Activity.RESULT_OK, i)
                            finish()
                        }
                    })
            }
            else -> {
                if (placeLoc != null) {
                    map?.moveCamera(CameraUpdateFactory.newLatLngZoom(placeLoc, DEFAULT_ZOOM))
                } else {
                    map?.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM))
                }
            }
        }

        addMyPlacesMarkers()

        map?.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener {
            val intent = Intent(this, ViewMyPlaceActivity::class.java)
            val i = markerPlaceIdMap[it]
            intent.putExtra("position", i)
            startActivity(intent)
            true
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if( state == SELECT_COORDINATES && !setCoorsEnabled) {
            menu?.add(0, 1, 1, "Select Coordinates")
            menu?.add(0, 2, 2, "Cancel")

        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            1 -> {
                setCoorsEnabled = true
                Toast.makeText(this, "Select coordinates", Toast.LENGTH_SHORT).show()
            }
            2 -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addMyPlacesMarkers() {
        val places = MyPlacesData.instance.getMyPlaces()
        val size = (places.size * 1.2F).toInt()
        markerPlaceIdMap = HashMap<Marker, Int>(size)

        for ( (i, place) in places.withIndex() ) {
            val loc = LatLng(place.latitude.toDouble(), place.longitude.toDouble())
            val markerOptions = MarkerOptions()
                    .position(loc)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.myplace))
                    .title(place.name)

            val marker = map?.addMarker(markerOptions)
            if (marker != null) {
                markerPlaceIdMap.put(marker, i)
            }


        }
    }

}
