package pw.alexbenic.myplaces

import android.app.Application
import android.content.Context

class MyPlacesApplication : Application() {

    init {
        context = this
    }

    companion object{
       var context : Context? = null
    }

}