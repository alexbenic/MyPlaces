package pw.alexbenic.myplaces

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // initalize toolbar
        val toolbar = find<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    // init menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    // handle menu item clicks
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId) {
            R.id.show_map_item -> {
                val i = Intent(this, MyPlacesMapActivity::class.java)
                i.putExtra("state", MyPlacesMapActivity.SHOW_MAP)
                startActivity(i)
            }
            R.id.new_place_item -> startActivity<EditMyPlaceActivity>()
            R.id.my_places_list_item -> startActivity<MyPlacesActivity>()
            R.id.about_item -> startActivity<AboutActivity>()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) toast("New place added!", Toast.LENGTH_LONG)
    }

    inline fun toast(message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, message, length).show()
    }

    inline fun <reified T : Activity> Context.startActivity() {
        val i = Intent(this, T::class.java)
        startActivity(i)
    }

    inline fun <reified T : View> Activity.find(id: Int) : T = findViewById(id) as T

}

