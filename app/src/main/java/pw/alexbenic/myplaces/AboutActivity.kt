package pw.alexbenic.myplaces

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        // init button
        val button = findViewById(R.id.about_ok) as Button
        button.setOnClickListener {
            finish()
        }
    }
}
