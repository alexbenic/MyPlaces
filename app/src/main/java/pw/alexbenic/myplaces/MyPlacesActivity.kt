package pw.alexbenic.myplaces

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MyPlacesActivity : AppCompatActivity() {

    val list by lazy { find<ListView>(R.id.my_places_list) }

    private var guiThread : Handler? = null
    private var context : Context? = null
    private var progressDialog : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_places)

        // set up toolbar
        val toolbar = find<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        // init thread
        guiThread = Handler()
        context = this
        progressDialog = ProgressDialog(this)

        // set up places array
        val myPlacesList = find<ListView>(R.id.my_places_list)

        val adapter = ArrayAdapter<MyPlace>(this,
                android.R.layout.simple_list_item_1,
                MyPlacesData.instance.getMyPlaces())
        myPlacesList.setAdapter(adapter)

        myPlacesList.setOnItemClickListener { _, _, i, _ ->
            val pos = Bundle()
            pos.putInt("position", i)
            val intent = Intent(this, ViewMyPlaceActivity::class.java)
            intent.putExtras(pos)
            startActivity(intent)
        }

        myPlacesList.setOnCreateContextMenuListener({
            menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo? ->
                val info : AdapterView.AdapterContextMenuInfo = menuInfo as AdapterView.AdapterContextMenuInfo
                val place = MyPlacesData.instance.getPlace(info.position)
                menu?.setHeaderTitle( place.name )
                menu?.add(0, 1, 1, "View place")
                menu?.add(0, 2, 2, "Edit place")
                menu?.add(0, 3, 3, "Delete place")
                menu?.add(0, 4, 4, "Show on map")
                menu?.add(0, 5, 5, "Upload place")
            }
        )
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        val info : AdapterView.AdapterContextMenuInfo = item?.menuInfo as AdapterView.AdapterContextMenuInfo
        val posBundle = Bundle()
        posBundle.putInt("position", info.position)
        when(item.itemId) {
            1 -> startActivity<ViewMyPlaceActivity>(posBundle)
            2 -> startActivity<EditMyPlaceActivity>(posBundle)
            3 -> {
                MyPlacesData.instance.deletePlace(info.position)
                setList()
            }
            4 -> {
                val place = MyPlacesData.instance.getPlace(info.position)
                val i = Intent(this, MyPlacesMapActivity::class.java)

                i.putExtra("state", MyPlacesMapActivity.CENTER_PLACE_ON_MAP)
                i.putExtra("lat", place.latitude)
                i.putExtra("lon", place.longitude)

                startActivityForResult(i, 2)

            }
            5 -> {
                val transThread : ExecutorService = Executors.newSingleThreadExecutor()
                transThread.submit {
                   val place = MyPlacesData.instance.getPlace(info.position)
                   guiStartProgressDialog("Sending place", "Sending ${place.name}")
                   try {
                       val m = MyPlacesHTTPHelper.sendMyPlace(place)
                       guiNotifyUser(m)
                   } catch (e : Exception) {
                       e.printStackTrace()
                   }
                   guiDismissProgressDialog()
                }
            }
        }


        return super.onContextItemSelected(item)
    }

    // init menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_my_places, menu)
        return true
    }

    // handle menu item clicks
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId) {
            R.id.show_map_item -> {
                val i = Intent(this, MyPlacesMapActivity::class.java)
                i.putExtra("state", MyPlacesMapActivity.SHOW_MAP)
                startActivity(i)
            }
            R.id.new_place_item -> startActivity<EditMyPlaceActivity>()
            R.id.server_list_item -> {
                val v : Dialog = MyPlacesServerList(this)
                v.setOnDismissListener {
                    setList()
                }
                v.show()
            }
            R.id.about_item -> startActivity<AboutActivity>()
        }

        return super.onOptionsItemSelected(item)
    }

    private  fun guiNotifyUser(message : String) {
        guiThread?.post{
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun guiStartProgressDialog(title : String, message : String) {
        guiThread?.post{
            // might break !!
            with (progressDialog!!) {
                setTitle(title)
                setMessage(message)
                show()
            }
        }
    }

    private fun guiDismissProgressDialog() {
        guiThread?.post {
            progressDialog?.dismiss()
        }
    }


    inline fun setList(){
        val adapter = ArrayAdapter<MyPlace>(this,
                android.R.layout.simple_list_item_1,
                MyPlacesData.instance.getMyPlaces())
        list.setAdapter(adapter)
    }

    inline fun <reified T : Activity> Context.startActivity() {
        val i = Intent(this, T::class.java)
        startActivity(i)
    }

    inline fun <reified T : View> Activity.find(id : Int) : T = findViewById(id) as T

    inline fun toast(message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(getApplicationContext(), message, length).show()
    }

    inline fun <reified T : Activity> Context.startActivity(bundle : Bundle) {
        val i = Intent(this, T::class.java)
        i.putExtras(bundle)
        startActivity(i)
    }

}
