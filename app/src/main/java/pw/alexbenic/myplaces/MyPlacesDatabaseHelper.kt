package pw.alexbenic.myplaces

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class MyPlacesDatabaseHelper(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int)
    : SQLiteOpenHelper(context, name, factory, version) {

    private val DATABASE_CREATE =
            "CREATE TABLE ${MyPlacesDBAdapter.DATABASE_TABLE} " +
                    "(" +
                    "${MyPlacesDBAdapter.PLACE_ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "${MyPlacesDBAdapter.PLACE_NAME} TEXT UNIQUE NOT NULL," +
                    "${MyPlacesDBAdapter.PLACE_DESCRIPTION} TEXT," +
                    "${MyPlacesDBAdapter.PLACE_LONG} TEXT," +
                    "${MyPlacesDBAdapter.PLACE_LAT} TEXT" +
                    ");"

    override fun onCreate(p0: SQLiteDatabase?) {

        try {
            p0?.execSQL(DATABASE_CREATE)
        } catch (e : SQLiteException) {
            Log.v("MyPlacesDatabaseHelper", e.message)
        }

    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val query = "DROP TABLE IF EXISTS ${MyPlacesDBAdapter.DATABASE_TABLE}"
        p0?.execSQL(query)
        onCreate(p0)
    }
}