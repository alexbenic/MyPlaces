package pw.alexbenic.myplaces

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MyPlacesServerList(val co : Context) : Dialog(co) {

    private val progressDialog : ProgressDialog

    init {
        progressDialog = ProgressDialog(co)
    }

//    private val progressDialog by lazy { ProgressDialog(co) }
//    private val guiThread by lazy { Handler() }
//    private val transThread : ExecutorService by lazy { Executors.newSingleThreadExecutor() }

    private var guiThread : Handler? = null
    private var transThread : ExecutorService? = null

    lateinit private var lv : ListView
    lateinit private var btn : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(R.string.myplaces_serverlist)
        setContentView(R.layout.serverlist)

        lv = find<ListView>(R.id.serverlist)
        btn = find<Button>(R.id.serverlist_done_button)
        guiThread = Handler()
        transThread = Executors.newSingleThreadExecutor()


        registerForContextMenu(lv)
        lv.setOnCreateContextMenuListener(this)
        btn.setOnClickListener {
            this.dismiss()
        }

        transThread?.submit {
            guiStartProgressDialog("Contacting server", "Getting available places")
            try {
                val names : List<String> = MyPlacesHTTPHelper.getAllPlacesNames()
                guiSetAdapter(names)
            } catch(e : Exception) {
                e.printStackTrace()
            }

            guiDismissProgressDialog()
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val info = menuInfo as AdapterView.AdapterContextMenuInfo
        val str = lv.getItemAtPosition(info.position) as String
        menu?.setHeaderTitle(str)
        menu?.add(0, 1, 1, "Download ${str}")
    }


    override fun onMenuItemSelected(featureId: Int, item: MenuItem?): Boolean {

        val info = item?.menuInfo as AdapterView.AdapterContextMenuInfo
        val name = lv.getItemAtPosition(info.position) as String

        transThread?.submit {
            guiStartProgressDialog("Downloading From Server", "Downloading ${name}")
            try {
                val place = MyPlacesHTTPHelper.getMyPlace(name)
                guiAddPlace(place)
            } catch( e : Exception) {
                e.printStackTrace()
                Log.d("Thread", e.toString())
            }
            guiDismissProgressDialog()
        }

        return super.onMenuItemSelected(featureId, item)
    }

    private fun guiAddPlace(place : MyPlace?) {
        guiThread?.post {
            if(place == null) {
                toast("Error during download")
            } else {
                MyPlacesData.instance.addNewPlace(place)
                toast("Downloaded place with name: ${place.name}")
            }

        }
    }

    private fun guiSetAdapter(names : List<String>) {
        guiThread?.post {
            Log.d("adapter", "${names[0]}")
            lv.adapter = ArrayAdapter<String>(co, android.R.layout.simple_list_item_1, names)
        }
    }

    private fun guiDismissProgressDialog() {
        guiThread?.post {
            progressDialog.dismiss()
        }
    }
    private fun guiStartProgressDialog(title : String, message : String) {
        guiThread?.post{
            // might break !!
//            with (progressDialog) {
//                setTitle(title)
//                setMessage(message)
//                show()
//            }
            progressDialog.setTitle(title)
            progressDialog.setMessage(message)
            progressDialog.show()
        }
    }

    fun toast(text : String, length : Int = Toast.LENGTH_SHORT) = Toast.makeText(context, text, length).show()
    inline fun <reified T : View> Dialog.find(id : Int) : T = findViewById(id) as T
}