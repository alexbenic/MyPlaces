package pw.alexbenic.myplaces

data class MyPlace(
        var name: String,
        var description : String = "",
        var longitude : String = "",
        var latitude : String = "",
        var ID : Long = 0L) {
    override fun toString(): String {
        return this.name
    }
}